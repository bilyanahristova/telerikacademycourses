const getGets = (arr) => {
  let index = 0;

  return () => {
    const toReturn = arr[index];
    index += 1;
    return toReturn;
  };
};
// this is the test
const test = [
  '3',
  '1',
  '2',
  '3',
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

//----------------------------------------
let numbersCount = +gets();

let numbers = new Array(numbersCount);

for (let i = 0; i < numbersCount; i++) {
  let currentNumber = +gets();
  numbers.push(currentNumber);
}

numbers.sort(function (a, b) {
  return b - a
}).slice(0, 3)

let largest = numbers[0];
let secondLargest = numbers[1];
let thirdLargest = numbers[2];

print(`${largest}, ${secondLargest} and ${thirdLargest}`);