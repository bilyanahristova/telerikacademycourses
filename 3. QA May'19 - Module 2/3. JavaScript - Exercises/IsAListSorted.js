let n = +gets();

for (let i = 0; i < n; i++) {
    let currentNumbers = gets().split(",").map(Number);

    let isSorted = true;

    for (let j = 1; j < currentNumbers.length; j++) {
        if (currentNumbers[j - 1] > currentNumbers[j]) {
            isSorted = false;
        }
    }

    print(isSorted);
}