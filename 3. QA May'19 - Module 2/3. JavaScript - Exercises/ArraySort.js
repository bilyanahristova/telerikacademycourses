let numbers = gets().split(",").map(Number);

let zeroes = [];

let index = 0;

while (numbers.includes(0)) {

    if (numbers[index] === 0) {
        numbers.splice(index, 1);
        zeroes.push(0);
        index = 0;
    } else {
        index++;
        continue;
    }
}

let sortedNumbers = numbers.concat(zeroes);
print(sortedNumbers);