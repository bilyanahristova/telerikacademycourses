let numbersCount = +gets();

let evenElementsProduct = 1;
let oddElementsProduct = 1;

for (let i = 1; i <= numbersCount; i++) {
    let currentNumber = +gets();

    if (i % 2 === 0) {
        evenElementsProduct *= currentNumber;
    } else {
        oddElementsProduct *= currentNumber;
    }
}

if (evenElementsProduct === oddElementsProduct) {
    print(`yes ${evenElementsProduct}`);
} else {
    print(`no ${oddElementsProduct} ${evenElementsProduct}`);
}