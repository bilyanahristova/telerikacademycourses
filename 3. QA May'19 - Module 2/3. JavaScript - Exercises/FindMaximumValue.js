let numbersCount = +gets();

let numbers = new Array(numbersCount);

for (let i = 0; i < numbersCount; i++) {
    let currentNumber = +gets();
    numbers.push(currentNumber);
}

let maximumValue = numbers.reduce(function(a, b) {
    return Math.max(a, b);
});

print(maximumValue);