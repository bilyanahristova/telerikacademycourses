let number = +gets();

let primeFactors = [];

for (i = 2; i <= number; i++) {
    while (number % i === 0) {
        primeFactors.push(i);
        number /= i;
    }
}

primeFactors.forEach(function (primeFactor) {
    print(primeFactor);
});