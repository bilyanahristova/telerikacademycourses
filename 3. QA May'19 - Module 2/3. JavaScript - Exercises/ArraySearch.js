let numbers = gets().split(",").map(Number);

let tempNumbers = [];
let missingNumbers = [];

for (let i = 0; i < numbers.length; i++) {
    tempNumbers[i] = i;
}

let distinctedNumbers = [...new Set(numbers)];

numbers = [];
numbers = distinctedNumbers;

for (let j = 1; j <= tempNumbers.length; j++) {
    if (!numbers.includes(j)) {
        missingNumbers.push(j);
    }
}

print(missingNumbers)