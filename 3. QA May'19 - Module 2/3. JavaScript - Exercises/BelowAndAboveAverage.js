
let numbers = gets().split(",").map(Number);

let belowAverageNumbers = [];
let aboveAverageNumbers = [];

let findAverage = numbers => numbers.reduce((a, b) => a + b, 0) / numbers.length;

let average = findAverage(numbers);

for (let i = 0; i < numbers.length; i++) {
    let currentNumber = numbers[i];

    if (currentNumber > average) {
        aboveAverageNumbers.push(currentNumber);
    } else if (currentNumber === 0 && average === 0) {
        continue;
    } else {
        belowAverageNumbers.push(currentNumber);
    }
}

print(`avg: ${average.toFixed(2)}`);
print(`below: ${belowAverageNumbers}`);
print(`above: ${aboveAverageNumbers}`);