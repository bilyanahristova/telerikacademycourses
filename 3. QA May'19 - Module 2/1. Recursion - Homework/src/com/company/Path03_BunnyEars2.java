package com.company;

import java.util.Scanner;

public class Path03_BunnyEars2 {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        int bunniesCount = user_input.nextInt();
        int bunniesEars = findBunnyEars(bunniesCount);

        System.out.println(bunniesEars);
    }

    private static int findBunnyEars(int bunniesCount) {
        int bunniesEars;

        if (bunniesCount <= 0) {
            return 0;
        }

        if (bunniesCount % 2 == 0) {
            bunniesEars = findBunnyEars(bunniesCount - 1) + 3;
        } else {
            bunniesEars = findBunnyEars(bunniesCount - 1) + 2;
        }

        return bunniesEars;
    }
}
