package com.company;

import java.util.Scanner;

public class Path06_CountHi {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        String input = user_input.nextLine();

        int hiCount = hiCounter(input);

        System.out.println(hiCount);
    }

    private static int hiCounter(String input) {
        if (input.length() < 2) {
            return 0;
        }

        if (input.endsWith("hi")) {
            return 1 + hiCounter(input.substring(0, input.length() - 2));
        }

        return hiCounter(input.substring(0, input.length() - 1));
    }
}
