package com.company;

import java.util.Scanner;

public class Path08_ChangePi {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        String str = user_input.nextLine();

        String pattern = "pi";
        String replacement = "3.14";

        String changedPi = replacePi(str, pattern, replacement);

        System.out.println(changedPi);

    }

    private static String replacePi(String string, String pattern, String replacement) {
        int index = string.indexOf(pattern);

        if (index < 0) {
            return string;
        }

        int endIndex = index + pattern.length();

        return string.substring(0, index) +
                replacement +
                replacePi(string.substring(endIndex), pattern, replacement);
    }
}
