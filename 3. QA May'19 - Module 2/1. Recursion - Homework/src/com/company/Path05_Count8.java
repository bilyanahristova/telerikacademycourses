package com.company;

import java.util.Scanner;

public class Path05_Count8 {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        int number = user_input.nextInt();

        int countEight = eightsCounter(number);

        System.out.println(countEight);
    }

    private static int eightsCounter(int number) {
        if (number <= 0) {
            return 0;
        }

        if (number % 10 == 8) {
            if (number % 100 == 88) {
                return 2 + eightsCounter(number / 10);
            }
            return 1 + eightsCounter(number / 10);
        }

        return eightsCounter(number / 10);
    }
}
