package com.company;

import java.util.Scanner;

public class Path04_Count7 {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        int number = user_input.nextInt();
        int countSeven = sevensCounter(number);

        System.out.println(countSeven);
    }

    private static int sevensCounter(int number) {
        if (number == 0) {
            return 0;
        }

        if (number % 10 == 7) {
            return 1 + sevensCounter(number / 10);
        }

        return sevensCounter(number / 10);
    }
}
