package com.company;

import java.util.Scanner;

public class Path01_Factorial {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        int number = user_input.nextInt();
        int factorial = findFactorial(number);

        System.out.println(factorial);
    }

    private static int findFactorial(int number) {
        int result;

        if (number == 1)
            return 1;

        result = findFactorial(number - 1) * number;
        return result;
    }
}
