package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Path07_Array6 {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        String[] input = user_input.next().split(",");

        ArrayList<Integer> numbers = new ArrayList<>();

        for (String number : input) {
            numbers.add(Integer.parseInt(number));
        }

        int index = user_input.nextInt();

        System.out.println(doesItContainsSix(numbers, index));

    }

    private static boolean doesItContainsSix(ArrayList<Integer> numbers, int index) {
        if (numbers.size() == 0 || index >= numbers.size()) {
            return false;
        }

        if (numbers.get(index) == 6) {
            return true;
        }

        return doesItContainsSix(numbers, index + 1);
    }
}
