Meta:
@TelerikForumTests

Narrative:
As a user
I want to perform an action (Login)
So that I can post in the forum

Scenario: User login the forum

Given Element logInButton is present
When Click logInButton element
And Type username in signInNameField field
And Type password in passwordInputField field
And Click signInButton element
Then Element homepageSchoolTelerikAcademy is present
