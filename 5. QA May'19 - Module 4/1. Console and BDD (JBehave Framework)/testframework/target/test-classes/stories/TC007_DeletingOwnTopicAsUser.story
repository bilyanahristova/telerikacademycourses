Meta:
@TelerikForumTests

Narrative:
As a user
I want to create a new topic
So that I can delete it

Scenario: As a user I want to create a new topic
Given Element threeDotsTopicArea is present
When Click homepageSchoolTelerikAcademy element
And Click createTopicButton element
And Type topicTitleTC7 in createTopicTitleField field
And Type topicTextTC7 in createTopicTextArea field
And Click createNewTopicButton element
And Element threeDotsTopicArea is present
And Click threeDotsTopicArea element
And Click deleteTopicButton element
And Element deletedTopicConfirmationMsg is present
Then Element threeDotsTopicArea is present