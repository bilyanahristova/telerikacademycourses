Meta:
@TelerikForumTests

Narrative:
As a user
I want to perform an actions
So that I can create a topic

Scenario: As a user I want to create a new topic
Given Element createTopicButton is present
When Click homepageSchoolTelerikAcademy element
And Click createTopicButton element
And Type topicTitleTC5 in createTopicTitleField field
And Type topicTextTC5 in createTopicTextArea field
And Click categoryDropDown element
And Click siteFeedbackCategory element
And Click createNewTopicButton element
Then Element threeDotsTopicArea is present