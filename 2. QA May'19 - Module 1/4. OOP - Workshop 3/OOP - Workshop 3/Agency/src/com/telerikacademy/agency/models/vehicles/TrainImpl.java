package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Train;

public class TrainImpl extends VehicleBase implements Train {
    //constants
    private static final int PASSENGER_MIN_CAPACITY = 30;
    private static final int PASSENGER_MAX_CAPACITY = 150;

    private static final int CARTS_MIN_CAPACITY = 1;
    private static final int CARTS_MAX_CAPACITY = 15;

    //exception messages
    private static final String PASSENGER_CAPACITY_EXCEPTION_MESSAGE = "A train cannot have less than 30 passengers or more than 150 passengers.";
    private static final String CARTS_CAPACITY_EXCEPTION_MESSAGE = "A train cannot have less than 1 cart or more than 15 carts.";

    //fields
    private int carts;

    //constructor
    public TrainImpl(int passengerCapacity, double pricePerKilometer, int carts) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
        setCarts(carts);
    }

    @Override
    public int getCarts() {
        return carts;
    }

    private void setCarts(int carts) {
        if (carts < CARTS_MIN_CAPACITY || carts > CARTS_MAX_CAPACITY) {
            throw new IllegalArgumentException(CARTS_CAPACITY_EXCEPTION_MESSAGE);
        }

        this.carts = carts;
    }

    //methods for min and max capacities
    @Override
    protected int defaultMinCapacity() {
        return PASSENGER_MIN_CAPACITY;
    }

    protected int defaultMaxCapacity() {
        return PASSENGER_MAX_CAPACITY;
    }

    protected String defaultCapacityExceptionMessage() {
        return PASSENGER_CAPACITY_EXCEPTION_MESSAGE;
    }

    public String print() {
        return "Train ----" + System.lineSeparator() +
                super.print() +
                "Carts amount: " + getCarts() +
                System.lineSeparator();
    }
}
