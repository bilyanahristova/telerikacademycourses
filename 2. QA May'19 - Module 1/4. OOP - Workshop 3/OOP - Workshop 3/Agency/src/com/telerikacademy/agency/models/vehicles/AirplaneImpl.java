package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Airplane;

public class AirplaneImpl extends VehicleBase implements Airplane {
    //fields
    private boolean hasFreeFood;

    //constructor
    public AirplaneImpl(int passengerCapacity, double pricePerKilometer, boolean hasFreeFood) {
        super(passengerCapacity, pricePerKilometer, VehicleType.AIR);
        setHasFreeFood(hasFreeFood);
    }

    private void setHasFreeFood(boolean hasFreeFood) {
        this.hasFreeFood = hasFreeFood;
    }

    @Override
    public boolean hasFreeFood() {
        return false;
    }

    public String print() {
        return "Airplane ----" + System.lineSeparator() +
                super.print() +
                "Has free food: " + hasFreeFood +
                System.lineSeparator();
    }
}
