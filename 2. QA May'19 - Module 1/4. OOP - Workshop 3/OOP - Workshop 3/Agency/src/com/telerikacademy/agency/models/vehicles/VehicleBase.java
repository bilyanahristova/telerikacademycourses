package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

public class VehicleBase implements Vehicle {

    //constants
    private static final int MIN_CAPACITY = 1;
    private static final int MAX_CAPACITY = 800;

    private static final double MIN_PRICE_PER_KILOMETER = 0.10;
    private static final double MAX_PRICE_PER_KILOMETER = 2.50;

    //exception messages
    private static final String CAPACITY_EXCEPTION_MESSAGE = "A vehicle with less than 1 passengers or more than 800 passengers cannot exist!";
    private static final String PRICE_PER_KILOMETER_EXCEPTION_MESSAGE = "A vehicle with a price per kilometer lower than $0.10 or higher than $2.50 cannot exist!";
    private static final String TYPE_NULL_EXCEPTION_MESSAGE = "Vehicle can`t be null";

    //fields
    int passengerCapacity;
    double pricePerKilometer;
    VehicleType type;

    //constructor
    public VehicleBase(int passengerCapacity, double pricePerKilometer, VehicleType type) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
        setType(type);
    }

    //getters
    @Override
    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    @Override
    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    @Override
    public VehicleType getType() {
        return type;
    }

    //setters
    private void setPassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < defaultMinCapacity() || passengerCapacity > defaultMaxCapacity()) {
            throw new IllegalArgumentException(defaultCapacityExceptionMessage());
        }

        this.passengerCapacity = passengerCapacity;
    }

    private void setPricePerKilometer(double pricePerKilometer) {
        if (pricePerKilometer < defaultMinPricePerKilometer() || pricePerKilometer > defaultMaxPricePerKilometer()) {
            throw new IllegalArgumentException(defaultPricePerKilometerExceptionMessage());
        }

        this.pricePerKilometer = pricePerKilometer;
    }

    private void setType(VehicleType type) {
        if (type == null) {
            throw new IllegalArgumentException(defaultTypeNullExceptionMessage());
        }

        this.type = type;
    }

    //methods for min and max capacities
    protected int defaultMinCapacity() { return MIN_CAPACITY; }

    protected int defaultMaxCapacity() { return MAX_CAPACITY; }

    //methods for default min and max prices per km
    protected double defaultMinPricePerKilometer() { return MIN_PRICE_PER_KILOMETER; }

    protected double defaultMaxPricePerKilometer() { return MAX_PRICE_PER_KILOMETER; }

    //methods for default exceptions
    protected String defaultCapacityExceptionMessage() { return CAPACITY_EXCEPTION_MESSAGE; }

    protected String defaultPricePerKilometerExceptionMessage() { return PRICE_PER_KILOMETER_EXCEPTION_MESSAGE; }

    protected String defaultTypeNullExceptionMessage() { return TYPE_NULL_EXCEPTION_MESSAGE; }

    @Override
    public String print() {
        return String.format("Passenger capacity: %d" + System.lineSeparator() +
                        "Price per kilometer: %.2f" + System.lineSeparator() +
                        "Vehicle type: %s" + System.lineSeparator(),
                getPassengerCapacity(),
                getPricePerKilometer(),
                getType().toString());
    }

    @Override
    public String toString() {
        return print();
    }
}
