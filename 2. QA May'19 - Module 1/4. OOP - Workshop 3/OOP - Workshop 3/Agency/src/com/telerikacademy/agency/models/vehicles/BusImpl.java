package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Bus;

public class BusImpl extends VehicleBase implements Bus {
    //constants
    private static final int PASSENGER_MIN_CAPACITY = 10;
    private static final int PASSENGER_MAX_CAPACITY = 50;

    //exception messages
    private static final String PASSENGER_CAPACITY_EXCEPTION_MESSAGE = "A bus cannot have less than 10 passengers or more than 50 passengers.";

    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
    }

    //methods for min and max capacities
    @Override
    protected int defaultMinCapacity() {
        return PASSENGER_MIN_CAPACITY;
    }

    protected int defaultMaxCapacity() {
        return PASSENGER_MAX_CAPACITY;
    }

    protected String defaultCapacityExceptionMessage() {
        return PASSENGER_CAPACITY_EXCEPTION_MESSAGE;
    }

    public String print() {
        return "Bus ----" +
                System.lineSeparator() +
                super.print();
    }
}
