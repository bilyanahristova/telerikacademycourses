package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Car;

public class CarImpl extends VehicleBase implements Car {
                        //constants
    //====================================================
    private final static int MIN_SEATS = 1;
    private final static int MAX_SEATS = 10;

                            //fields
    //====================================================
    private int seats;

                        //constructor
    //====================================================
    public CarImpl(String make, String model, double price, int seats) {
        super(make, model, price, VehicleType.CAR);
        setSeats(seats);
    }

    public int getSeats() {
        return seats;
    }

    private void setSeats(int seats) {
        if(seats < MIN_SEATS || seats > MAX_SEATS){
            throw new IllegalArgumentException("Seats must be between 1 and 10!");
        }
        this.seats = seats;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  Seats: %d", getSeats());
    }
}
