package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Utils;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

public abstract class VehicleBase implements Vehicle {
                        //constants
    //====================================================
    private final static int MIN_MAKE_LENGTH = 2;
    private final static int MAX_MAKE_LENGTH = 15;
    private final static int MIN_MODEL_LENGTH = 1;
    private final static int MAX_MODEL_LENGTH = 15;
    public final static double MIN_PRICE = 0.0;
    public final static double MAX_PRICE = 1000000.0;

    private final static String MAKE_FIELD = "Make";
    private final static String MODEL_FIELD = "Model";
    private final static String PRICE_FIELD = "Price";
    private final static String WHEELS_FIELD = "Wheels";
    private final static String COMMENTS_HEADER = "    --COMMENTS--";
    private final static String NO_COMMENTS_HEADER = "    --NO COMMENTS--";

    //add fields
    private String make;
    private String model;
    private double price;
    private VehicleType type;
    private int wheels;
    private List<Comment> comments;

    // finish the constructor and validate input; look in package com.telerikacademy.dealership.models.common.enums; what methods are there in VehicleType?
    VehicleBase(String make, String model, double price, VehicleType type) {
        setMake(make);
        setModel(model);
        setPrice(price);
        setType(type);
        this.wheels = getWheels();

        comments = new ArrayList<>();
    }



                            //getters
    //====================================================
    @Override
    public String getMake() {
        return make;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public VehicleType getType() {
        return type;
    }

    @Override
    public List<Comment> getComments() {
        return comments;
    }
    //====================================================



                            //setters
    //====================================================
    private void setMake(String make) {
        if (make.length() < MIN_MAKE_LENGTH || make.length() > MAX_MAKE_LENGTH) {
            throw new IllegalArgumentException("Make must be between 2 and 15 characters long!");
        }

        this.make = make;
    }

    private void setModel(String model) {
        if (model.length() < MIN_MODEL_LENGTH || model.length() > MAX_MODEL_LENGTH){
            throw new IllegalArgumentException("Model must be between 1 and 15 characters long!");
        }

        this.model = model;
    }

    private void setPrice(double price) {
        if (price < MIN_PRICE || price > MAX_PRICE) {
            throw new IllegalArgumentException("Price must be between 0.0 and 1000000.0!");
        }

        this.price = price;
    }

    private void setType(VehicleType type) {
        if (type == null) {
            throw new IllegalArgumentException("Vehicle cannot be null!");
        }
        this.type = type;
    }

    private void setWheels(int wheels) {
        this.wheels = wheels;
    }

    private void setComments(List<Comment> comments) {
        this.comments = comments;
    }
    //====================================================



                    //implemented methods
    //====================================================
    @Override
    public int getWheels() {
        return type.getWheelsFromType();
    }

    @Override
    public void removeComment(Comment comment) {
        if (comments == null) {
            throw new IllegalArgumentException("Cannot remove comment! The comment does not exist!");
        }
        comments.remove(comment);

    }

    @Override
    public void addComment(Comment comment) {
        comments.add(comment);
    }
    //====================================================



                        //print methods
    //====================================================
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("%s:", this.getClass().getSimpleName().replace("Impl", ""))).append(System.lineSeparator());
        //finish implementation of toString() - what should the next 3 lines of code append to the builder?
        builder.append(String.format("  %s: %s", MAKE_FIELD, getMake())).append(System.lineSeparator());
        builder.append(String.format("  %s: %s", MODEL_FIELD, getModel())).append(System.lineSeparator());
        builder.append(String.format("  %s: %d", WHEELS_FIELD, getWheels())).append(System.lineSeparator());

        builder.append(String.format("  %s: $%s", PRICE_FIELD, Utils.removeTrailingZerosFromDouble(price))).append(System.lineSeparator());

        if (!printAdditionalInfo().isEmpty()) {
            builder.append(printAdditionalInfo()).append(System.lineSeparator());
        }
        builder.append(printComments());
        return builder.toString();
    }

    //todo replace this comment with explanation why this method is protected:
    protected abstract String printAdditionalInfo();

    private String printComments()
    {
        StringBuilder builder = new StringBuilder();

        if (comments.size() <= 0)
        {
            builder.append(String.format("%s", NO_COMMENTS_HEADER));
        }
        else
        {
            builder.append(String.format("%s", COMMENTS_HEADER)).append(System.lineSeparator());

            for (Comment comment : comments)
            {
                builder.append(comment.toString());
            }

            builder.append(String.format("%s", COMMENTS_HEADER));
        }

        return builder.toString();
    }
    //====================================================
}
