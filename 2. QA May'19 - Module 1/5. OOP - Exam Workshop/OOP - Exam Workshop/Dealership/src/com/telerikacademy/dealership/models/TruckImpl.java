package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Truck;

public class TruckImpl extends VehicleBase implements Truck {
                        //constants
    //====================================================
    private final static int MIN_CAPACITY = 1;
    private final static int MAX_CAPACITY = 100;

                            //fields
    //====================================================
    private int weightCapacity;

                        //constructor
    //====================================================
    public TruckImpl(String make, String model, double price, int weightCapacity) {
        super(make, model, price, VehicleType.TRUCK);
        setWeightCapacity(weightCapacity);
    }

    @Override
    public int getWeightCapacity() {
        return weightCapacity;
    }

    private void setWeightCapacity(int weightCapacity) {
        if (weightCapacity < MIN_CAPACITY|| weightCapacity > MAX_CAPACITY) {
            throw new IllegalArgumentException("Weight capacity must be between 1 and 100!");
        }

        this.weightCapacity = weightCapacity;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  Weight Capacity: %d", getWeightCapacity()) + "t";
    }



}
