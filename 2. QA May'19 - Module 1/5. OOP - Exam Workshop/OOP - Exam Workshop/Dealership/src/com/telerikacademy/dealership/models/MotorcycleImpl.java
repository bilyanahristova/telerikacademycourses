package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Motorcycle;

public class MotorcycleImpl extends VehicleBase implements Motorcycle {
                        //constants
    //====================================================
    public final static int MIN_CATEGORY_LENGTH = 3;
    public final static int MAX_CATEGORY_LENGTH = 10;

                            //fields
    //====================================================
    private String category;

                        //constructor
    //====================================================
    public MotorcycleImpl(String make, String model, double price, String category) {
        super(make, model, price, VehicleType.MOTORCYCLE);
        setCategory(category);
    }


    @Override
    public String getCategory() {
        return category;
    }

    private void setCategory(String category) {
        if (category.length() < MIN_CATEGORY_LENGTH || category.length() > MAX_CATEGORY_LENGTH) {
            throw new IllegalArgumentException("Category must be between 3 and 10 characters long!");
        }

        this.category = category;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  Category: %s", getCategory());
    }
}
