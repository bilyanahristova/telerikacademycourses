package tests.models;

import com.telerikacademy.dealership.models.MotorcycleImpl;
import com.telerikacademy.dealership.models.contracts.Motorcycle;
import com.telerikacademy.dealership.models.contracts.Vehicle;
import org.junit.Assert;
import org.junit.Test;

public class MotorcycleImpl_Tests {

    @Test
    public void MotorcycleImpl_ShouldImplementMotorcycleInterface() {
        MotorcycleImpl motorcycle = new MotorcycleImpl("make", "model", 100, "category");
        Assert.assertTrue(motorcycle instanceof Motorcycle);
    }

    @Test
    public void MotorcycleImpl_ShouldImplementVehicleInterface() {
        MotorcycleImpl motorcycle = new MotorcycleImpl("make", "model", 100, "category");
        Assert.assertTrue(motorcycle instanceof Vehicle);
    }

    @Test(expected = NullPointerException.class)
    public void Constructor_ShouldThrow_WhenMakeIsNull() {
        MotorcycleImpl motorcycle = new MotorcycleImpl(null, "model", 100, "category");
    }

    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenMakeLengthIsBelow2() {
        MotorcycleImpl motorcycle = new MotorcycleImpl("m", "model", 100, "category");
    }

    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenMakeLengthIsAbove15() {
        MotorcycleImpl motorcycle = new MotorcycleImpl("makeMakeMakeMakeMake", "model", 100, "category");
    }

    @Test(expected = NullPointerException.class)
    public void Constructor_ShouldThrow_WhenModelIsNull() {
        MotorcycleImpl motorcycle = new MotorcycleImpl("make", null, 100, "category");
    }

    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenModelLengthIsBelow_1() {
        MotorcycleImpl motorcycle = new MotorcycleImpl("make", "", 100, "category");
    }

    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenModelLengthIsAbove15() {
        MotorcycleImpl motorcycle = new MotorcycleImpl("make", "modelModelModelM", 100, "category");
    }

    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenPriceIsNegative() {
        MotorcycleImpl motorcycle = new MotorcycleImpl("make", "model", -100, "category");
    }

    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenPriceIsAbove1000000() {
        MotorcycleImpl motorcycle = new MotorcycleImpl("make", "model", 1000001, "category");
    }

    @Test(expected = NullPointerException.class)
    public void Constructor_ShouldThrow_WhenCategoryIsNull() {
        MotorcycleImpl motorcycle = new MotorcycleImpl("make", "model", 100, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenCategoryLengthIsBelow_3() {
        MotorcycleImpl motorcycle = new MotorcycleImpl("make", "model", 100, "dr");
    }

    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenCategoryLengthIsAbove10() {
        MotorcycleImpl motorcycle = new MotorcycleImpl("make", "model", 100, "12345678901");
    }
}
