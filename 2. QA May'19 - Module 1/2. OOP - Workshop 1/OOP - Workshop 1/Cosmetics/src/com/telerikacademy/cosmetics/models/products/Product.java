package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;

public class Product {
    //fields
    private static final int NAME_MIN_LENGTH = 3;
    private static final int NAME_MAX_LENGTH = 10;
    private static final int BRAND_MIN_LENGTH = 2;
    private static final int BRAND_MAX_LENGTH = 10;

    private String name;
    private String brand;
    private double price;
    private GenderType gender;

    //constructor
    public Product(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        setGender(gender);
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        if (name.length() < NAME_MIN_LENGTH || name.length() > NAME_MAX_LENGTH) {
            throw new IllegalArgumentException("Name should be between 3 and 10 symbols.");
        }

        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    private void setBrand(String brand) {
        if (brand.length() < BRAND_MIN_LENGTH || brand.length() > BRAND_MAX_LENGTH) {
            throw new IllegalArgumentException("Brand should be between 2 and 10 symbols.");
        }

        this.brand = brand;
    }

    public double getPrice() {
        return price;
    }

    private void setPrice(double price) {
        if (price < 0) {
            throw new IllegalArgumentException("Price must be positive.");
        }

        this.price = price;
    }

    public GenderType getGender() {
        return gender;
    }

    private void setGender(GenderType gender) {
        this.gender = gender;
    }


    public String print() {
        return String.format(" #%s %s\n" +
                " #Price: $%.2f\n" +
                " #Gender: %s\n" +
                " ===",
                name,
                brand,
                price,
                gender);
    }

    public String toString() {
        return print();
    }
}
