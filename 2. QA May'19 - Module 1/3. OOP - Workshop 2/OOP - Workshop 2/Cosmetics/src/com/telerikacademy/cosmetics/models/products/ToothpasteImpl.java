package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Toothpaste;

import java.util.ArrayList;
import java.util.List;

public class ToothpasteImpl extends ProductBase implements Toothpaste {
    //field
    private List<String> ingredients;

    //constructor
    public ToothpasteImpl(String name, String brand, double price, GenderType gender, List<String> ingredients) {
        super(price, name, brand, gender);

        setIngredients(ingredients);
        if (ingredients == null){
            throw new IllegalArgumentException("Ingredients cannot be null");
        }

        ingredients = new ArrayList<>();
    }

    @Override
    //getter with deep copy of the array list
    public List<String> getIngredients() {
        return new ArrayList<>(ingredients);
    }

    private void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }

    public String print() {
        return super.print() + String.format(" #Ingredients: %s\r\n" + " ===", getIngredients());
    }
}
