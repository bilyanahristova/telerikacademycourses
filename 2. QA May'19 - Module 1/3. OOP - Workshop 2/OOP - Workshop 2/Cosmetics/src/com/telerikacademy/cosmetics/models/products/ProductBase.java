package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Product;

public class ProductBase implements Product {
    //fields
    public static final int NAME_MIN_LENGTH = 3;
    public static final int NAME_MAX_LENGTH = 10;

    public static final int BRAND_MIN_LENGTH = 2;
    public static final int BRAND_MAX_LENGTH = 10;

    private double price;
    private String name;
    private String brand;
    private GenderType gender;

    //constructor:
    public ProductBase(double price, String name, String brand, GenderType gender) {
        this.setPrice(price);
        this.setName(name);
        this.setBrand(brand);
        this.setGender(gender);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getBrand() {
        return brand;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public GenderType getGender() {
        return gender;
    }

    @Override
    public String print() {
        return String.format("#%s %s\n" +
                " #Price: $%.2f\n" +
                " #Gender: %s\n",
                getName(),
                getBrand(),
                getPrice(),
                getGender());
    }

    private void setPrice(double price) {
        if (price < 0.0) {
            throw new IllegalArgumentException("Price cannot be negative");
        }
            this.price = price;
        }

        private void setName (String name){

        if (name == null || name.trim().isEmpty()){
            throw new IllegalArgumentException("Name cannot be null.");
        }

            if (name.length() < NAME_MIN_LENGTH || name.length() > NAME_MAX_LENGTH) {
                throw new IllegalArgumentException("Name should be between 3 - 10 symbols.");
            }

            this.name = name;
        }

        private void setBrand (String brand){
            if (brand == null || brand.trim().isEmpty()){
                throw new IllegalArgumentException("Brand cannot be null.");
            }

            if (brand.length() < BRAND_MIN_LENGTH || brand.length() > BRAND_MAX_LENGTH) {
                throw new IllegalArgumentException("Brand should be between 2 - 10 symbols.");
            }

            this.brand = brand;
        }

        private void setGender (GenderType gender) {
            this.gender = gender;
        }
    }
