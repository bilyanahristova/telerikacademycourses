package com.telerikacademy.cosmetics.models;


import com.telerikacademy.cosmetics.models.contracts.Category;
import com.telerikacademy.cosmetics.models.contracts.Product;

import java.util.ArrayList;
import java.util.List;

public class CategoryImpl implements Category {
    //fields
    public static final int NAME_MIN_LENGTH = 3;
    public static final int NAME_MAX_LENGTH = 15;

    private String name;
    private List<Product> products;

    //constructor
    public CategoryImpl(String name) {
        setName(name);
        products = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        if (name == null || name.trim().isEmpty()){
            throw new IllegalArgumentException("Name cannot be null.");
        }

        if (name.length() < NAME_MIN_LENGTH || name.length() > NAME_MAX_LENGTH) {
            throw new IllegalArgumentException("Name should be between 3 - 10 symbols.");
        }

        this.name = name;
    }

    public List<Product> getProducts() {
        //we return a copy because we dont want to override real values of products.
        return new ArrayList<>(products);
    }

    public void addProduct(Product product) {
        if (product == null) {
            throw new IllegalArgumentException("The product cannot be null.");
        }

        products.add(product);
    }

    public void removeProduct(Product product) {
        if (product == null) {
            throw new IllegalArgumentException("The product cannot be null.");
        }

        if (!products.contains(product)) {
            throw new IllegalArgumentException("The product must exist.");
        }

        products.remove(product);
    }

    public String print() {
        if (products.size() == 0) {
            return String.format("#Category: %s\n" +
                            " #No product in this category", name);
        }

        StringBuilder output = new StringBuilder();

        output.append(String.format("#Category: %s\n", name));

        for (Product product : products) {
            output.append(product.print());
        }

        return output.toString();
    }
}
