package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.ScentType;
import com.telerikacademy.cosmetics.models.contracts.Cream;

public class CreamImpl extends ProductBase implements Cream {
    //fields
    private ScentType scent;

    //constructor
    public CreamImpl(String name, String brand, double price, GenderType gender, ScentType scent) {
        super(price, name, brand, gender);
        setScent(scent);
    }

    @Override
    public ScentType getScent() {
        return scent;
    }

    private void setScent(ScentType scent) {
        this.scent = scent;
    }

    public String print() {
        return super.print() + String.format(" #Scent: %s\r\n" + " ===", getScent());
    }
}
