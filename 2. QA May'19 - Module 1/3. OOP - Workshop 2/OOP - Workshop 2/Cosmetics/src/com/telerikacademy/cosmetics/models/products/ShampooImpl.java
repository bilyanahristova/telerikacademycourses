package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Shampoo;


public class ShampooImpl extends ProductBase implements Shampoo {
    //fields
    private int  milliliters;
    private UsageType usageType;

    //constructor
    public ShampooImpl(String name, String brand, double price, GenderType gender, int milliliters, UsageType usageType) {
        super(price, name, brand, gender);
        this.setMilliliters(milliliters);
        this.setUsageType(usageType);
    }

    @Override
    public int getMilliliters() {
        return milliliters;
    }

    @Override
    public UsageType getUsage() {
        return (usageType);
    }

    private void setMilliliters(int milliliters) {
        if (milliliters < 0) {
            throw new IllegalArgumentException("Milliliters cannot be negative");
        }
        this.milliliters = milliliters;
    }

    private void setUsageType(UsageType usageType) {
        this.usageType = usageType;
    }

    @Override
    public String print() {
        return super.print() + String.format(" #Milliliters: %s\r\n" + " #Usage: %s\r\n" + " ===", getMilliliters(), getUsage());
    }
}
