import java.util.ArrayList;
import java.util.Scanner;

public class ThreeGroups {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        String[] input = user_input.nextLine().split(" ");

        ArrayList<Integer> numbers = new ArrayList<>();

        for (String number : input) {
            numbers.add(Integer.parseInt(number));
        }

        ArrayList<Integer> firstGroup = new ArrayList<>();
        ArrayList<Integer> secondGroup = new ArrayList<>();
        ArrayList<Integer> thirdGroup = new ArrayList<>();

        for (int i = 0; i < numbers.size(); i++) {
            int currentNumber = numbers.get(i);

            if (currentNumber % 3 == 0) {
                firstGroup.add(currentNumber);
            } else if (currentNumber % 3 == 1) {
                secondGroup.add(currentNumber);
            } else if (currentNumber % 3 == 2) {
                thirdGroup.add(currentNumber);
            }
        }

        StringBuilder output = new StringBuilder();

        if (firstGroup.size() != 0) {
            for (int number : firstGroup) {
                output.append(number + " ");
            }
            output.append("\n");
        } else {
            output.append("\n");
        }

        if (secondGroup.size() != 0) {
            for (int number : secondGroup) {
                output.append(number + " ");
            }
            output.append("\n");
        } else {
            output.append("\n");
        }

        if (thirdGroup.size() != 0) {
            for (int number : thirdGroup) {
                output.append(number + " ");
            }
            output.append("\n");
        } else {
            output.append("\n");
        }

        System.out.println(output);
    }
}
