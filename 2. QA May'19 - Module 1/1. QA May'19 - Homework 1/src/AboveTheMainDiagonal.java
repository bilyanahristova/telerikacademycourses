import java.util.Scanner;

public class AboveTheMainDiagonal {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        int numberOfRowsAndColumns = user_input.nextInt();

        long[][] matrix = new long[numberOfRowsAndColumns][numberOfRowsAndColumns];

        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[row].length; col++) {
                matrix[row][col] = (long)Math.pow(2, row+col);
            }
        }

        long sumOfTheNumbersAboveMainDiagonal = 0;

        for (int j = 1; j < numberOfRowsAndColumns; j++) {
            for (int k = j-1; k >= 0; k--) {
                sumOfTheNumbersAboveMainDiagonal += matrix[k][j];
            }
        }

        System.out.println(sumOfTheNumbersAboveMainDiagonal);
    }
}