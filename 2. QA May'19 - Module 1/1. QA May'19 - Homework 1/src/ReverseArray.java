import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class ReverseArray {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int[] array = Arrays
                .stream(reader.readLine().split("\\s"))
                .mapToInt(Integer::parseInt)
                .toArray();

        for (int i = 0; i < array.length / 2; i++) {
            int temp = array[i];
            array[i] = array[array.length - 1 - i];
            array[array.length - 1 - i] = temp;
        }

        if (array.length >= 1) {
            System.out.print(array[0]);
        }

        for (int i = 1; i < array.length; i++) {
            System.out.print(", " + array[i]);
        }
    }
}
