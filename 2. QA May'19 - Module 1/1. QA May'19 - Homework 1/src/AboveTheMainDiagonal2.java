import java.util.Scanner;

public class AboveTheMainDiagonal2 {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        int numberOfRowsAndColumns = user_input.nextInt();

        long[][] matrix = new long[numberOfRowsAndColumns][numberOfRowsAndColumns];

        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[row].length; col++) {
                matrix[row][col] = (long)Math.pow(2, row+col);
            }
        }

        long sumOfTheNumbers = 0;

        for (int j = 1; j < numberOfRowsAndColumns; j++) {
            for (int k = j-1; k >= 0; k--) {
                sumOfTheNumbers += matrix[k][j];
            }
        }

        for (int i = 0; i < numberOfRowsAndColumns; i++) {
            for (int j = 0; j < numberOfRowsAndColumns; j++) {
                if (i == j) {
                    sumOfTheNumbers += matrix[i][j];
                }
            }
        }

        System.out.println(sumOfTheNumbers);
    }
}
