import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class SymmetricArrays {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        int arraysCount = Integer.parseInt(user_input.nextLine());

        ArrayList<String> outputs = new ArrayList<>();

        for (int i = 0; i < arraysCount; i++) {
            String input = user_input.nextLine();

            int[] currentArray = Arrays
                    .stream(input.split(" "))
                    .mapToInt(Integer::parseInt)
                    .toArray();

            String currentOutput = "Yes";

            for (int k = 0; k < (currentArray.length + 1) / 2; k++) {
                if (currentArray[k] != currentArray[currentArray.length - k - 1]) {
                    currentOutput = "No";
                }
            }

            if (currentOutput.equals("Yes")) {
                outputs.add("Yes");
            } else {
                outputs.add("No");
            }
        }

        outputs.forEach(System.out::println);
    }
}