import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class FindMaximumValue {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        int numbersCount = user_input.nextInt();
        ArrayList<Integer> numbers = new ArrayList<>();

        for (int i = 0; i < numbersCount; i++) {
            int currentNumber = user_input.nextInt();
            numbers.add(currentNumber);
        }

        int maxValue = Collections.max(numbers);

        System.out.println(maxValue);
    }
}