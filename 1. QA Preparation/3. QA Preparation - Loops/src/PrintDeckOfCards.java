import java.util.Arrays;
import java.util.Scanner;

public class PrintDeckOfCards {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        String cardSign = user_input.nextLine();

        String[] cards =  { "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A" };

        for (int i = 0; i <= Arrays.asList(cards).indexOf(cardSign); i++) {
            System.out.printf("%s of spades, %s of clubs, %s of hearts, %s of diamonds \n",
                    cards[i], cards[i], cards[i], cards[i]);
        }
    }
}