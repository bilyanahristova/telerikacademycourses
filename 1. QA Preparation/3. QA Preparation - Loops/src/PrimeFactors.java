import java.util.ArrayList;
import java.util.Scanner;

public class PrimeFactors {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        int number = user_input.nextInt();

        ArrayList<Integer> primeFactors = new ArrayList<>();

        while (number % 2 == 0) {
            primeFactors.add(2);
            number = number / 2;
        }

        for (int i = 3; i < Math.sqrt(number); i = i + 2) {
            while (number % i == 0) {
                primeFactors.add(i);
                number = number / i;
            }
        }

        if (number > 2) {
            primeFactors.add(number);
        }

        for (int primeFactor : primeFactors) {
            System.out.println(primeFactor);
        }
    }
}
