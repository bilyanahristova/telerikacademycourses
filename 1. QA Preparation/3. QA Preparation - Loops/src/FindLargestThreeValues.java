import java.util.Arrays;
import java.util.Scanner;

public class FindLargestThreeValues {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        int numbersCount = user_input.nextInt();
        int[] numbers = new int[numbersCount];

        for (int i = 0; i < numbersCount; i++) {
            int currentNumber = user_input.nextInt();
            numbers[i] = currentNumber;
        }

        Arrays.sort(numbers);

        System.out.printf("%d, %d and %d",
                numbers[numbers.length - 1],
                numbers[numbers.length - 2],
                numbers[numbers.length - 3]);
    }
}