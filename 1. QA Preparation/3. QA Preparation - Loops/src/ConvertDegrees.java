import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;

public class ConvertDegrees {
    public static void main(String[] args) throws IOException {
        Scanner user_input = new Scanner(System.in);
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int[] temperatures = Arrays
                .stream(reader.readLine().split("\\s"))
                .mapToInt(Integer::parseInt)
                .toArray();

        for (int temperature: temperatures) {
            int convertedTemperature = Math.round((temperature * 9) / 5 + 32);
            System.out.println(convertedTemperature);
        }
    }
}