import java.io.Console;
import java.util.Scanner;

public class NotDivisableNumber {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        int n = user_input.nextInt();

        for (int i = 1; i <= n; i++) {

            if (!(i % 3 == 0 || i % 7 == 0))
            {
                System.out.printf(i + " ");
            }
        }
    }
}