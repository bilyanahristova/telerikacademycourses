import java.util.Scanner;

public class From1ToN {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        int n = user_input.nextInt();

        for (int i = 1; i <= n; i++) {
            System.out.printf(i + " ");
        }
    }
}