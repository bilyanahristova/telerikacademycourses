import java.math.BigDecimal;
import java.util.Scanner;

public class CalculateComplexSum {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        int n = user_input.nextInt();
        int x = user_input.nextInt();

        double temp = 1;
        double sequenceSum = 0;

        for (int i = 1; i <= n; i++)
        {
            if (i == 1)
            {
                sequenceSum += 1;
                sequenceSum += 1 / x;
            }

            temp = temp * i;

            sequenceSum += (temp / Math.pow(x, i));
        }

        System.out.printf("%.5f", sequenceSum);
    }
}
