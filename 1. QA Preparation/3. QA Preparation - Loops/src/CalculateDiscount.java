import java.util.Scanner;

public class CalculateDiscount {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        int itemsCount = user_input.nextInt();

        double discountedPrice;

        for (int i = 0; i < itemsCount; i++) {
            double currentItemPrice = user_input.nextDouble();

            double currentDiscout = 0.65d * currentItemPrice;

             discountedPrice = currentItemPrice - currentDiscout;

            System.out.printf("%.2f \n", discountedPrice);
        }
    }
}