import java.util.Scanner;

public class MinMaxSumAverage {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        int numbersCount = user_input.nextInt();

        double sum = 0;
        double minValue = Double.MAX_VALUE;
        double maxValue = Double.MIN_VALUE;

        for (int i = 0; i < numbersCount; i++) {
            double currentNumber = user_input.nextDouble();
            sum += currentNumber;

            if (currentNumber < minValue) {
                minValue = currentNumber;
            }

            if (currentNumber > maxValue) {
                maxValue = currentNumber;
            }
        }

        double average = sum / numbersCount;

        System.out.printf("min=%.2f", minValue);
        System.out.println();
        System.out.printf("max=%.2f", maxValue);
        System.out.println();
        System.out.printf("sum=%.2f", sum);
        System.out.println();
        System.out.printf("avg=%.2f", average);
    }
}