import java.util.Scanner;

public class MatrixNumbers {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        int n = user_input.nextInt();

        for (int i = 1; i <= n; i++) {
            StringBuilder result = new StringBuilder();

            for (int j = i; j < i + n; j++) {
                result.append(j + " ");
            }

            System.out.println(result.toString());
        }
    }
}