import java.util.Scanner;

public class FindAverage {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        int numbersCount = user_input.nextInt();

        double sum = 0;

        for (int i = 0; i < numbersCount; i++) {
            double currentNumber = user_input.nextDouble();
            sum += currentNumber;
        }

        double average = sum / numbersCount;

        System.out.printf("%.2f", average);
    }
}