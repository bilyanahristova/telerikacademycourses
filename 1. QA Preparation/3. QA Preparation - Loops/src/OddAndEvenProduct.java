import java.util.Scanner;

public class OddAndEvenProduct {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        int numbersCount = user_input.nextInt();

        int evenElementsProduct = 1;
        int oddElementProduct = 1;

        for (int i = 1; i <= numbersCount; i++) {
            int currentNumber = user_input.nextInt();

            if (i % 2 == 0) {
                evenElementsProduct *= currentNumber;
            }

            else {
                oddElementProduct *= currentNumber;
            }
        }

        if (evenElementsProduct == oddElementProduct) {
            System.out.printf("yes %d \n", evenElementsProduct);
        }

        else {
            System.out.printf("no %d %d \n", oddElementProduct, evenElementsProduct);
        }
    }
}
