import java.util.Scanner;

public class ChineseZodiac {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        int year = user_input.nextInt();

        String zodiacSign = null;

        year = Math.abs(year % 12);

        switch(year)
        {
            case 0:
                zodiacSign = "Monkey";
                break;
            case 1:
                zodiacSign = "Rooster";
                break;
            case 2:
                zodiacSign = "Dog";
                break;
            case 3:
                zodiacSign = "Pig";
                break;
            case 4:
                zodiacSign = "Rat";
                break;
            case 5:
                zodiacSign = "Ox";
                break;
            case 6:
                zodiacSign = "Tiger";
                break;
            case 7:
                zodiacSign = "Hare";
                break;
            case 8:
                zodiacSign = "Dragon";
                break;
            case 9:
                zodiacSign = "Snake";
                break;
            case 10:
                zodiacSign = "Horse";
                break;
            case 11:
                zodiacSign = "Sheep";
                break;
        }

        System.out.println(zodiacSign);
    }
}
