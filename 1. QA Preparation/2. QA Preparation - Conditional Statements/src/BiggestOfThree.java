import java.math.BigDecimal;
import java.util.Scanner;

public class BiggestOfThree {

    public static void main(String[] args) {

        Scanner user_input = new Scanner(System.in);

        double[] numbers = new double[3];

        for (int j = 0; j < 3; j++) {
            String number = user_input.nextLine();
            double parsedNumber = Double.parseDouble(number);
            numbers[j] = parsedNumber;
        }

        double biggestNumber = numbers[0];

        for (int i = 0; i < numbers.length; i++) {
            if (biggestNumber < numbers[i]) {
                biggestNumber = numbers[i];
            }
        }

        System.out.println(removeTrailingZerosFromDouble(biggestNumber));
    }

    public static String removeTrailingZerosFromDouble(double number) {
        BigDecimal num = BigDecimal.valueOf(number).stripTrailingZeros();
        return num.toPlainString();
    }
}