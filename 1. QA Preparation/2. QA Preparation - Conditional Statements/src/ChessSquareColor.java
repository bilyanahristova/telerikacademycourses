import java.util.Scanner;

public class ChessSquareColor {

    public static void main(String[] args) {

        Scanner user_input = new Scanner(System.in);

        char file = user_input.nextLine().charAt(0);
        int rank = user_input.nextInt();

        int parsedFile = file;

        if (rank % 2 == 0)
        {
            if (parsedFile % 2 == 0)
            {
                System.out.println("light");
            }

            else
            {
                System.out.println("dark");
            }
        }

        else
        {
            if (parsedFile % 2 == 0)
            {
                System.out.println("light");
            }

            else
            {
                System.out.println("dark");
            }
        }
    }
}
