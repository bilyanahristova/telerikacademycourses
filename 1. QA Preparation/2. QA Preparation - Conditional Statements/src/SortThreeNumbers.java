import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class SortThreeNumbers {

    public static void main(String[] args) {

        Scanner user_input = new Scanner(System.in);

        int firstNumber = user_input.nextInt();
        int secondNumber = user_input.nextInt();
        int thirdNumber = user_input.nextInt();

        ArrayList<Integer> numbers = new ArrayList<Integer>();
        numbers.add(firstNumber);
        numbers.add(secondNumber);
        numbers.add(thirdNumber);

        Collections.sort(numbers, Collections.reverseOrder());

        for (int i = 0; i < numbers.size(); i++) {
            System.out.print(numbers.get(i) + " ");
        }
    }
}
