import java.util.Scanner;

public class PhoneBill {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        double totalAmountOfTextMessages = user_input.nextDouble();
        double totalAmountOfMinutes = user_input.nextDouble();

        double additionalMessages = Math.abs(totalAmountOfTextMessages - 20);
        double additionalMessagesTax = additionalMessages * 0.06;

        double additionalMinutes = Math.abs(totalAmountOfMinutes - 60);
        double additionalMinutesTax = additionalMinutes  * 0.10;

        double messagesSalesTax = additionalMessagesTax  * 0.20;
        double minutesSalesTax = additionalMinutesTax  * 0.20;

        double salesTax = messagesSalesTax + minutesSalesTax ;

        double totalMessagesTax=  additionalMessagesTax + messagesSalesTax ;
        double totalMinutesTax = additionalMinutesTax + minutesSalesTax ;

        double totalBill = totalMessagesTax + totalMinutesTax + 12.00;

        if (totalAmountOfTextMessages > 20 && totalAmountOfMinutes > 60) {
            System.out.printf("%.0f additional messages for %.2f levas \n", additionalMessages, additionalMessagesTax);
            System.out.printf("%.0f additional minutes for %.2f levas \n", additionalMinutes, additionalMinutesTax);
            System.out.printf("%.2f additional taxes \n", salesTax);
            System.out.printf("%.2f total bill \n", totalBill);

        } else if (totalAmountOfTextMessages <= 20 && totalAmountOfMinutes > 60) {
            totalBill = additionalMinutesTax  + minutesSalesTax  + 12.00;

            System.out.print("0 additional messages for 0.00 levas \n");
            System.out.printf("%.0f additional minutes for %.2f levas \n", additionalMinutes, additionalMinutesTax);
            System.out.printf("%.2f additional taxes \n", minutesSalesTax);
            System.out.printf("%.2f total bill \n", totalBill);

        } else if (totalAmountOfTextMessages > 20 && totalAmountOfMinutes <= 60) {
            totalBill = additionalMessagesTax  + messagesSalesTax  + 12.00;

            System.out.printf("%.0f additional messages for %.2f levas \n", additionalMessages, additionalMessagesTax);
            System.out.print("0 additional minutes for 0.00 levas \n");
            System.out.printf("%.2f additional taxes \n", messagesSalesTax);
            System.out.printf("%.2f total bill \n", totalBill);

        } else {
            System.out.println("0 additional messages for 0.00 levas");
            System.out.println("0 additional minutes for 0.00 levas");
            System.out.println("0.00 additional taxes");
            System.out.println("12.00 total bill");
        }
    }
}