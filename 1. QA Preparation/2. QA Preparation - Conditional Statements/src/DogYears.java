import java.util.Scanner;

public class DogYears {

    public static void main(String[] args) {

        Scanner user_input = new Scanner(System.in);

        int humanYearsNumber = user_input.nextInt();

        int dogYearsNumber = 0;

        switch (humanYearsNumber) {
            case 1:
                dogYearsNumber = humanYearsNumber * 10;
                break;

            case 2:
                dogYearsNumber = humanYearsNumber * 10;
                break;

            default:
                int reminder = humanYearsNumber - 2;
                dogYearsNumber = 2 * 10 + reminder * 4;
                break;
        }

        System.out.println(dogYearsNumber);
    }
}
