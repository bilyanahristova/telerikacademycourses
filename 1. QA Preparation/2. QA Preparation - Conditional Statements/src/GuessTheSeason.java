import java.util.Scanner;

public class GuessTheSeason {

    public static void main(String[] args) {

        Scanner user_input = new Scanner(System.in);

        String month = user_input.nextLine();
        int date = user_input.nextInt();

        String season = new String();

        switch (month)
        {
            case "March":
                if (date >= 1 && date <= 19)
                {
                    season = "Winter";
                }

                else if (date >= 20 && date <= 31)
                {
                    season = "Spring";
                }
                break;

            case "April":
                if (date >= 1 && date <= 30)
                {
                    season = "Spring";
                }
                break;

            case "May":
                if (date >= 1 && date <= 31)
                {
                    season = "Spring";
                }
                break;

            case "June":
                if (date >= 1 && date <= 20)
                {
                    season = "Spring";
                }

                else if (date >= 21 && date <= 30)
                {
                    season = "Summer";
                }
                break;

            case "July":

            case "August":
                if (date >= 1 && date <= 31)
                {
                    season = "Summer";
                }
                break;

            case "September":
                if (date >= 1 && date <= 21)
                {
                    season = "Summer";
                }

                else if (date >= 22 && date <= 30)
                {
                    season = "Autumn";
                }
                break;

            case "October":
                if (date >= 1 && date <= 31)
                {
                    season = "Autumn";
                }
                break;

            case "November":
                if (date >= 1 && date <= 30)
                {
                    season = "Autumn";
                }
                break;

            case "December":
                if (date >= 1 && date <= 20)
                {
                    season = "Autumn";
                }

                else if (date >= 21 && date <= 31)
                {
                    season = "Winter";
                }
                break;

            case "January":
                if (date >= 1 && date <= 31)
                {
                    season = "Winter";
                }
                break;

            case "February":
                if (date >= 1 && date <= 29)
                {
                    season = "Winter";
                }
                break;
        }

        System.out.println(season);
    }
}
