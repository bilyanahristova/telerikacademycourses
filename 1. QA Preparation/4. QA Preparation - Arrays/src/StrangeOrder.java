import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StrangeOrder {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        String[] input = user_input.next().split(",");

        ArrayList<Integer> numbers = new ArrayList<>();

        for (String number : input) {
            numbers.add(Integer.parseInt(number));
        }

        ArrayList<Integer> rightSide = new ArrayList<>();
        ArrayList<Integer> middle = new ArrayList<>();
        ArrayList<Integer> leftSide= new ArrayList<>();

        for (int i = 0; i < numbers.size(); i++) {
            int currentNumber = numbers.get(i);

            if (currentNumber < 0) {
                rightSide.add(currentNumber);
            }
            else if (currentNumber == 0) {
                middle.add(currentNumber);
            }
            else if (currentNumber > 0) {
                leftSide.add(currentNumber);
            }
        }

        ArrayList<Integer> strangeOrdered = (ArrayList<Integer>) Stream.of(rightSide, middle, leftSide)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        for (int j = 0; j < strangeOrdered.size(); j++) {
            if (j == strangeOrdered.size() - 1) {
                System.out.print(strangeOrdered.get(j));
            } else {
                System.out.print(strangeOrdered.get(j) + ",");
            }
        }
    }
}
