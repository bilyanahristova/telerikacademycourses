import java.util.ArrayList;
import java.util.Scanner;

public class CombineLists {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        String[] firstArray = user_input.nextLine().split(",");
        Integer[] parsedFirstArray = new Integer[firstArray.length];

        String[] secondArray = user_input.nextLine().split(",");
        Integer[] parsedSecondArray = new Integer[secondArray.length];

        ArrayList<Integer> combinedList = new ArrayList<>();

        for (int i = 0; i < firstArray.length; i++) {
            parsedFirstArray[i] = new Integer(firstArray[i]);
        }

        for (int i = 0; i < secondArray.length; i++) {
            parsedSecondArray[i] = new Integer(secondArray[i]);
        }

        for (int i = 0; i < firstArray.length; i++) {
            combinedList.add(parsedFirstArray[i]);
            combinedList.add(parsedSecondArray[i]);
        }

        for (int j = 0; j < combinedList.size(); j++) {
            if (j == combinedList.size() - 1) {
                System.out.print(combinedList.get(j));
            } else {
                System.out.print(combinedList.get(j) + ",");
            }
        }
    }
}