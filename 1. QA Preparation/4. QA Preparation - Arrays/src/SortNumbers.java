import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class SortNumbers {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        String[] numbers = user_input.nextLine().split(", ");

        Integer[] intArray = new Integer[numbers.length];

        for (int i = 0; i < numbers.length; i++) {
            intArray[i] = new Integer(numbers[i]);
        }

        Arrays.sort(intArray, Collections.reverseOrder());
        for (int i = 0; i <numbers.length; i++) {
            if (i == numbers.length - 1) {
                System.out.print(intArray[i]);
            }
            else {
                System.out.print(intArray[i]+ ", ");
            }
        }
    }
}
