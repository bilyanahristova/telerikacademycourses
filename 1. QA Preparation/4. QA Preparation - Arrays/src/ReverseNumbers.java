import java.util.Scanner;

public class ReverseNumbers {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        String[] numbers = user_input.nextLine().split(", ");

        Integer[] intArray = new Integer[numbers.length];

        for (int i = 0; i < numbers.length; i++) {
            intArray[i] = new Integer(numbers[i]);
        }

        for (int i = intArray.length - 1; i  >= 0; i--) {
            if (i == 0) {
                System.out.print(intArray[i]);
            }
            else {
                System.out.print(intArray[i]+ ", ");
            }
        }
    }
}
