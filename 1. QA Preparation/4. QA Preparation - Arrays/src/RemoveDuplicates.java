import java.util.*;

public class RemoveDuplicates {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        String[] elements = user_input.next().split(",");

        ArrayList<String> listOfElements = new ArrayList<>(Arrays.asList(elements));

        for (String element : elements) {
            listOfElements.add(element);
        }

        Set<String> set = new LinkedHashSet<>(listOfElements);

        listOfElements.clear();
        listOfElements.addAll(set);

        for (int j = 0; j < listOfElements.size(); j++) {
            if (j == listOfElements.size() - 1) {
                System.out.print(listOfElements.get(j));
            } else {
                System.out.print(listOfElements.get(j) + ",");
            }
        }
    }
}
