import java.util.ArrayList;
import java.util.Scanner;
import java.util.stream.Collectors;

public class BelowAndAboveAverage {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        String[] numbers = user_input.next().split(",");

        ArrayList<Integer> parsedNumbers = new ArrayList<>();

        for (String number : numbers) {
            parsedNumbers.add(Integer.parseInt(number));
        }

        double sum = 0;

        for (int i = 0; i < parsedNumbers.size(); i++) {
            sum += parsedNumbers.get(i);
        }

        double average = sum / parsedNumbers.size();

        ArrayList<Integer> belowAverageNumbers = new ArrayList<>();
        ArrayList<Integer> aboveAverageNumbers = new ArrayList<>();

        for (int i = 0; i < parsedNumbers.size(); i++) {
            int currentNumber = parsedNumbers.get(i);

            if (currentNumber > average) {
                aboveAverageNumbers.add(currentNumber);
            }
            else if (currentNumber == 0 && average == 0) {
                continue;
            }
            else {
                belowAverageNumbers.add(currentNumber);
            }
        }

        System.out.printf("avg: %.2f \n", average);

        System.out.printf("below: %s \n", belowAverageNumbers
                .stream()
                .map(Object::toString)
                .collect(Collectors.joining(",")));

        System.out.printf("above: %s \n", aboveAverageNumbers
                .stream()
                .map(Object::toString)
                .collect(Collectors.joining(",")));
    }
}
