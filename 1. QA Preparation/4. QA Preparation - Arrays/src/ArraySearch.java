import java.util.*;

public class ArraySearch {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        String[] input = user_input.next().split(",");

        ArrayList<Integer> numbers = new ArrayList<>();

        for (String number : input) {
            numbers.add(Integer.parseInt(number));
        }

        int[] tempNumbers = new int[numbers.size()];

        ArrayList<Integer> missingNumbers = new ArrayList<>();

        for (int i = 0; i < numbers.size(); i++) {
            tempNumbers[i] = i;
        }

        Set<Integer> distinctedNumbers = new LinkedHashSet<>(numbers);

        numbers.clear();
        numbers.addAll(distinctedNumbers);

        for (int i = 1; i <= tempNumbers.length; i++) {
            if (!numbers.contains(i)) {
                missingNumbers.add(i);
            }
        }

        for (int j = 0; j < missingNumbers.size(); j++) {
            if (j == missingNumbers.size() - 1) {
                System.out.print(missingNumbers.get(j));
            } else {
                System.out.print(missingNumbers.get(j) + ",");
            }
        }
    }
}
