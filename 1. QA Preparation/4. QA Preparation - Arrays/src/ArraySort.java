import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ArraySort {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        String[] numbers = user_input.next().split(",");

        ArrayList<Integer> parsedNumbers = new ArrayList<>();

        for (String number : numbers) {
            parsedNumbers.add(Integer.parseInt(number));
        }

        ArrayList<Integer> zeroes = new ArrayList<>();

        int index = 0;

        while (parsedNumbers.contains(0)) {

            if (parsedNumbers.get(index).equals(0)) {
                parsedNumbers.remove(index);
                zeroes.add(0);
                index = 0;
            }
            else {
                index++;
                continue;
            }
        }

        List<Integer> sortedNumbers = Stream
                .concat(parsedNumbers.stream(), zeroes.stream())
                .collect(Collectors.toList());

        for (int i = 0; i < sortedNumbers.size(); i++) {
            if (i == sortedNumbers.size() - 1) {
                System.out.print(sortedNumbers.get(i));
            } else {
                System.out.print(sortedNumbers.get(i) + ",");
            }
        }
    }
}
