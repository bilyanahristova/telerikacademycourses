import java.util.*;

public class IsAListSorted {
    public static void main(String[] args) {
        Scanner user_input = new Scanner(System.in);

        int n = user_input.nextInt();

        for (int i = 0; i < n; i++) {
            String[] numbers = user_input.next().split(",");

            ArrayList<Integer> parsedNumbers = new ArrayList<>();

            for (String number : numbers) {
                parsedNumbers.add(Integer.parseInt(number));
            }

            boolean isSorted = true;

            for (int j = 1; j < parsedNumbers.size(); j++) {
                if (parsedNumbers.get(j - 1) > (parsedNumbers.get(j))) {
                    isSorted = false;
                }
            }

            System.out.println(isSorted);
        }
    }
}
