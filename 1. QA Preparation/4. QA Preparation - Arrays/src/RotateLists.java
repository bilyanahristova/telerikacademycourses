import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class RotateLists {
    public static void main(String[] args) {
        Scanner user_input = new Scanner (System.in);

        String[] numbers = user_input.next().split(",");

        int n = user_input.nextInt();

        Collections.rotate(Arrays.asList(numbers),-n);
        System.out.println(Arrays.toString(numbers)
                .replace("[","")
                .replace("]","")
                .replace(" ", ""));
    }
}