import java.util.Scanner;

public class Interest {

    public static void main(String[] args) {

        Scanner user_input = new Scanner(System.in);

        int depositedSum = user_input.nextInt();

        double firstYearInterest = (depositedSum * 0.05) + depositedSum;
        double secondYearInterest = (firstYearInterest * 0.05) + firstYearInterest;
        double thirdYearInterest = (secondYearInterest * 0.05) + secondYearInterest;

        System.out.printf("%.2f", firstYearInterest);
        System.out.println();
        System.out.printf("%.2f", secondYearInterest);
        System.out.println();
        System.out.printf("%.2f", thirdYearInterest);
    }
}
