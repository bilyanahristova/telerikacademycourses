import java.util.Scanner;

public class Tips {

    public static void main(String[] args) {

        Scanner user_input = new Scanner(System.in);

        int orderedMealPrice = user_input.nextInt();

        int tip = orderedMealPrice / 10;

        int totalSum = orderedMealPrice + tip;

        System.out.println(totalSum);
    }
}
