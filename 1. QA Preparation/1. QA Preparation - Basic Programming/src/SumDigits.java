import java.util.Scanner;

public class SumDigits {

    public static void main(String[] args) {

        Scanner user_input = new Scanner(System.in);

        int number = user_input.nextInt();

        int firstDigit = number / 1000;
        int secondDigit = (number / 100) % 10;
        int thirdDigit = (number % 100) / 10;
        int fourthDigit = number % 10;

        int digitsSum = firstDigit + secondDigit + thirdDigit + fourthDigit;

        System.out.println(digitsSum);
    }
}
