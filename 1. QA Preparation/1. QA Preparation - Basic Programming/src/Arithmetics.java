import java.util.Scanner;

public class Arithmetics {

    public static void main(String[] args) {

        Scanner user_input = new Scanner(System.in);

        int firstNumber = user_input.nextInt();
        int secondNumber = user_input.nextInt();

        int sum = firstNumber + secondNumber;
        int substractedDifference = firstNumber - secondNumber;
        int product = firstNumber * secondNumber;
        int remainder = firstNumber % secondNumber;
        int powered = (int)Math.pow(firstNumber, secondNumber);

        System.out.println(sum);
        System.out.println(substractedDifference);
        System.out.println(product);
        System.out.println(remainder);
        System.out.println(powered);
    }
}
