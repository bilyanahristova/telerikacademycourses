import java.util.Scanner;

public class SumPositive {

    public static void main(String[] args) {

        Scanner user_input = new Scanner(System.in);

        int number = user_input.nextInt();

        int numbersSum = number * (number + 1) / 2;

        System.out.println(numbersSum);
    }
}
