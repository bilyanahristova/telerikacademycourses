import java.util.Scanner;

public class Converter {

    public static void main(String[] args) {

        Scanner user_input = new Scanner(System.in);

        int milesPerGalon = user_input.nextInt();

        double gallonLitresCapacity = 4.54;
        double milesToKilometers = 1.6;

        double kilometersDriven = milesPerGalon * milesToKilometers;
        double kilometersPerLiter = kilometersDriven / gallonLitresCapacity;

        int litersPer100Kilometers = (int)Math.floor(100 * (gallonLitresCapacity / kilometersDriven));

        System.out.printf("%d litres per 100 km", litersPer100Kilometers);
    }
}
