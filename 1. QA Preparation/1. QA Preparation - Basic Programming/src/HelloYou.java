import java.util.Scanner;

public class HelloYou {

    public static void main(String[] args) {

        Scanner user_input = new Scanner(System.in);

        String firstName = user_input.nextLine();

        System.out.println("Hello, " + firstName + "!");
    }
}
