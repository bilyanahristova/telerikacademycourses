import java.util.Scanner;

public class RectangleArea {

    public static void main(String[] args) {

        Scanner user_input = new Scanner(System.in);

        int rectangleWidth = user_input.nextInt();
        int rectangleHeight = user_input.nextInt();

        int rectangleArea = rectangleWidth * rectangleHeight;

        System.out.println(rectangleArea);
    }
}
