import java.util.Scanner;

public class BottleDeposit {

    public static void main(String[] args) {

        Scanner user_input = new Scanner(System.in);

        int halfLiterBottlesNumber = user_input.nextInt();
        int LiterBottlesNumber = user_input.nextInt();

        double halfLiterBottlesDeposit = 0.1;
        double LiterBottlesDeposit = 0.25;

        double totalSum = (halfLiterBottlesDeposit * halfLiterBottlesNumber) + (LiterBottlesDeposit * LiterBottlesNumber);

        System.out.printf("%.2f", totalSum);
    }
}
