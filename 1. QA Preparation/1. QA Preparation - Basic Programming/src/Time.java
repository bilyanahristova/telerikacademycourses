import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Time {

    public static void main(String[] args) {

        Scanner user_input = new Scanner(System.in);

        int days = user_input.nextInt();
        int hours = user_input.nextInt();
        int minutes = user_input.nextInt();
        int seconds = user_input.nextInt();

        int daysToSeconds = (int)TimeUnit.DAYS.toSeconds(days);
        int hoursToSeconds = (int)TimeUnit.HOURS.toSeconds(hours);
        int minutesToSeconds = (int)TimeUnit.MINUTES.toSeconds(minutes);

        int totalSeconds = daysToSeconds + hoursToSeconds + minutesToSeconds + seconds;

        System.out.println(totalSeconds);
    }
}
