# Software Testing Basics - Homework
***

### 1. Find as many bugs as you can on the Bug Example image in the resources.

1. Twitter logo/icon doesn't show;
2. Unnecessary tabulation in login via twitter button;
3. Facebook in the button must be with capital letter;
4. There must be a button for forgotten password;
5. Login button is too big and the text is not synchronized with the size of the button;
6. "Log-in" it is not written correctly/must be "Login";
7. Sign up button have to be at the bottom or at least not to the title of the box;
8. Login text is writed in different ways in different positions;
9. "****" in password field should be replaced with other text, for example "Enter a password...";
***

### 2. Create test cases covering the Comments and Creation of topics functionalities of a forum:

* **1. Comments**

    **1.1 Title: Create new comment for a topic**

		Narrative: 
		* I want to create new comment for a topic as a registered user.
		
		Steps to reproduce:
		1. Log in with my account.
		2. Open a topic.
		3. Click on the "Reply" button.
		4. Typing some text in text area.
		5. Click on the "Reply" button.
		
	**1.2 Title: Edit a comment**

		Narrative: 
		* I want to edit a comment as a registered user and saving the changes.
		
		Steps to reproduce:
		1. Log in with my account.
		2. Open the topic where i post my comment.
		3. Click on the "Edit comment" button.
		3. Make some changes (example: text or adding/deleting some details).
		4. Click on the "Save changes" button.
		
	**1.3 Title: Delete a comment**

		Narrative: 
		* I want to delete a comment as a registered user.
		
		Steps to reproduce:
		1. Log in with my account.
		2. Open the topic where i post my comment.
		3. Click on the "Delete comment" button.
		
		
* **2. Creation of topics**

    **2.1 Title: Create new topic**

		Narrative: 
		* I want to create new topic as a registered user.
		
		Steps to reproduce:
		1. Log in with my account.
		2. Click on the button for creating new topic.
		3. Filling the title and Typing some text in the text area.
		4. Click on the "Create new topic" button.
		
	**2.2 Title: Edit a topic**

		Narrative: 
		* I want to edit a topic as a registered user.
		
		Steps to reproduce:
		1. Log in with my account.
		2. Click on the button for editing the topic.
		3. Make some changes (example: title, text or adding/deleting some details).
		4. Click on the "Edit"/"Save" button.
		
	**2.3 Title: Delete a topic**

		Narrative: 
		* I want to delete a topic as a registered user.
		
		Steps to reproduce:
		1. Log in with my account.
		2. Going to the existing topic.
		3. Click on "Delete this topic" button.

***

### 3. Prioritize the test cases for using a microwave. Use priority levels from 1 to 3, where 1 means highest priority.
* Power goes off while the microwave is working. - **Prio 1**
* The display shows accurate time. - **Prio 2**
* The microwave starts when the start button is pressed. - **Prio 1**
* The microwave stops when the stop button is pressed. - **Prio 1**
* Selection of program changes the mode of the microwave. - **Prio 2**
* The light inside turns on when the door is opened. - **Prio 3**