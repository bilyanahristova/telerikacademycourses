package testCases;

import org.junit.Test;

public class TelerikSchoolForumSearchTest extends BaseTest {
	String searchCriterion = "Test 30";

	@Test
	public void TelerikSchoolForumSearch() {
		actions.clickElement("search.Button");
		actions.typeValueInField(searchCriterion, "search.Input");
	}
}
