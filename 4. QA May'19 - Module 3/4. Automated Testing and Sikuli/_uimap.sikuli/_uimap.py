########################################################
# UI map for XYZ
########################################################
from sikuli import *
########################################################

class telerikForumUI:
    new_topic_button = "new_topic_button.png"
    title_input_field = "title_input_field.png"
    text_input_field = "text_input_field.png"
    create_new_topic_button = "create_new_topic_button.png"
    new_topic_created_assert = "new_topic_created_assert.png"
