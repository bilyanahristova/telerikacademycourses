from sikuli import *
import unittest
import iHTMLTestRunner.iHTMLTestRunner as HTMLTestRunner
from _uimap import *

class OpenMozilla:
	@classmethod
	def Start(self):
		type('r', KeyModifier.WIN)
		sleep(5)
		type('firefox')
		sleep(5)
		type(Key.ENTER)
		sleep(5)
		type('https://schoolforum.telerikacademy.com/' + Key.ENTER)
		sleep(5)

	@classmethod
	def Close(self):
		type(Key.F4, KeyModifier.ALT)

