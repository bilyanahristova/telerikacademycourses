from _lib import *

    
class SmokeTests(unittest.TestCase):
    
    def setUp(self):
        pass
    
    def tearDown(self):
        pass    
        
    def test_001_OpenMozilla(self):
        OpenMozilla.Start()
        
    def test_002_Create_New_Topic(self):
        click(telerikForumUI.new_topic_button)
        sleep(1)
        click(telerikForumUI.title_input_field)
        sleep(2)
        type("Another test with sikuli framework")
        click(telerikForumUI.text_input_field)
        sleep(2)
        type("Another test with sikuli framework")
        click(telerikForumUI.create_new_topic_button)
        sleep(5)

        assert(exists(telerikForumUI.new_topic_created_assert).highlight(2))
            

    def test_003_CloseCalc(self):
        OpenMozilla.Close()

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(SmokeTests)
    
   
    outfile = open("Report.html", "w")
    runner = HTMLTestRunner.HTMLTestRunner(stream=outfile, title='SmokeTests Report' )
    runner.run(suite)
    outfile.close()

