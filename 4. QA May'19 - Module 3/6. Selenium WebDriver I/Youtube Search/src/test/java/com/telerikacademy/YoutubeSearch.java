package com.telerikacademy;

import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.sql.Driver;

public class YoutubeSearch {
    private WebDriver driver;
    private String baseURL = "https://www.youtube.com/";
    private WebDriverWait wait;

    @Before
    public void setUp() {
         System.setProperty("webdriver.gecko.driver", "resources/geckodriver.exe");
         driver = new FirefoxDriver();

//        System.setProperty("webdriver.chrome.driver", "resources/chromedriver.exe");
//        driver = new ChromeDriver();

        driver.get(baseURL);
        driver.manage().window().maximize();
        wait = new WebDriverWait(driver,100);
    }

    @Test
    public void youtubeSearch() throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='search']")));

        WebElement searchField = driver.findElement(By.xpath("//input[@id='search']"));

        searchField.sendKeys("Eminem - Space Bound");
        searchField.sendKeys(Keys.ENTER);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@title='Eminem - Space Bound']")));

        driver.findElement(By.xpath("//a[@title='Eminem - Space Bound']")).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//yt-formatted-string[text()='Eminem - Space Bound']")));

        WebElement playButton = driver.findElement(By.xpath("//button[contains(@class, 'ytp-play-button')]"));

        playButton.sendKeys(Keys.chord("k"));

        WebElement fullScreenButton = driver.findElement(By.xpath("//button[contains(@class, 'ytp-fullscreen-button')]"));

        fullScreenButton.sendKeys(Keys.chord("f"));

        wait.until(ExpectedConditions.elementToBeClickable(fullScreenButton));

        Thread.sleep(100);

        driver.findElement(By.xpath("//button[contains(@class, 'ytp-fullscreen-button')]")).sendKeys(Keys.chord("f"));
    }

    @After
    public void quitDriver() {
        driver.quit();
    }
}
